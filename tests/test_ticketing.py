# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Fanalytical Inc..

"""Test Accounts."""

import json
import os

import pytest
from helpers import ordered
from jsonschema import ValidationError, validate

from translations import ticketing


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ({}, {}),  # External Ids
    ],
)
def test_fields(test_input, expected):
    """General simple field test.

    The first element of the list is the dictionary we receive from the source,
    i.e. ``{'Full_Account_ID__c': '0011U00000Nfvg9QAB'}`` and the second is the
    output from the translation i.e.
    ``{'external_ids': [{'origin': 'sf', 'id': '0011U00000Nfvg9QAB'}]}``.
    """
    # Add default fields to the expected value
    res = ticketing.translate(test_input)
    if res:
        # Remove field added by post-process
        assert res.pop('scores', None) is not None
        assert (
            res.pop('organization_id')
            == 'test-uuid'
        )
    assert ordered(res) == ordered(expected)


def test_account_valid_info():
    """Test schema validation."""
    pwd = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(pwd, 'ticketing.json')) as f:
        schema = json.load(f)

    with pytest.raises(ValidationError):
        validate({}, schema)

    raw = {
    }

    account = ticketing.translate(raw)

    validate(account, schema)
