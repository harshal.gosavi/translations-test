# -*- coding: utf-8 -*-
#
# Copyright (C) 2019, 2020, 2021 Fanalytical Inc..

"""Common data wrangling functions to clean, modify and create client data."""

import copy
import re
from datetime import datetime, timedelta
from typing import Any, Dict, List, Union

from more_itertools import unique_everseen


def remove_duplicates_from_list(
    data: Dict[str, Any], *keys: str
) -> Dict[str, Any]:
    """Remove duplicates from a list preserving order.

    :param data: Dictionary like object to extract the lists from.
    :param keys: List of keys to remove duplicates from.

    :return: Copy of data with the duplicated removed from the lists under the
      keys.
    """
    d = copy.deepcopy(data)
    for key in keys:
        if key not in d:
            continue
        d[key] = [e for i, e in enumerate(d[key]) if e and e not in d[key][:i]]

        # Remove the key if nothing is left
        if not d[key]:
            del d[key]

    return d


def create_external_id(source: str, id_: str) -> str:
    """Generate a string representing the external ID using the parameters.

    :param source: Source of origin of the identifier, i.e. salesforce,
      paciolan, etc.
    :param id_: The identifier.

    :return: String ``source:id_``.
    """
    return f'{source}:{id_}'


def basic_fix_string_value(value: str) -> Union[str, None]:
    r"""Clean up of string values.

    - Remove leading and trailing whitespace.
    - Remove line breaks and take only unique lines

    >>> from translations.wrangling import basic_fix_string_value
    >>> basic_fix_string_value('3102 Oak Lawn Ave.\n3102 Oak Lawn Ave.\n725')
    '3102 Oak Lawn Ave. 725'
    """
    return (
        ' '.join(unique_everseen(value.strip().split('\n'))).title()
        if value
        else None
    )


email_regexp = re.compile(r'[^@]+@[^@]+\.[^@]+')
"""e-mail matching regular expression.

We consider that an email is valid if it has exactly one @ sign, and at least
one . in the part after the @.
"""


def is_valid_email(value: str) -> bool:
    """Check if an email is correct.

    :param value: Email string
    :return: the ``value`` if it is a valid mail, ``None`` otherwise.
    """
    return True if value and email_regexp.match(value) else False


def clean_phone_number(value: Union[str, None]) -> Union[str, None]:
    """Clean phone number to contain only digits."""
    value = ''.join([str(s) for s in value if s.isdigit()]) if value else None
    return None if not value or len(set(value)) == 1 else value


def clean_address(
    address: Union[None, List[Dict[str, Any]]]
) -> Union[None, List[Dict[str, Any]]]:
    """Clean up account addresses list.

    - Remove empty fields.
    - Remove empty addresses.
    - Merge duplicates.

    :param address: List of addresses to clean up.

    :return: List of addresses cleaned up or None.
    """
    if not address:
        return None

    def is_valid_string(s):
        """Check if the string inside the address info is correct."""
        if s in (None, 'NA', '**'):
            return False
        return True

    # Filter None values
    for i, d in enumerate(address):
        address[i] = dict((k, v) for k, v in d.items() if is_valid_string(v))
    # Filter empty addresses
    address = [d for d in address if d]
    if not address:
        return None
    # Use merger to remove duplicates
    # merger = AccountMerger({})
    # return merger.merge_address(address[:1], address[1:])
    zip_codes = set()
    res = []
    for a in address:
        zip_code = a.get('postal_code')
        if zip_code and zip_code not in zip_codes:
            res.append(a)
            zip_codes.add(zip_code)
    return res


def postprocess_account(
    account: Dict[str, Any], *args, **kwargs
) -> Dict[str, Any]:
    """Fix account information.

    - Clean up address list
    - Remove duplicates from phone and email.
    """
    # Fix addresses
    account['address'] = clean_address(account.get('address'))
    if not account['address']:
        del account['address']
    # Remove duplicates
    account = remove_duplicates_from_list(account, 'phone', 'email')
    # Add empty scores field for future use
    account['scores'] = {}

    return account


def from_excel_ordinal(ordinal, _epoch0=datetime(1899, 12, 31)):
    """Extract a datetime object from an excel ordinal date.

    Excel ordinal are dates stored as seconds since 1900/01/01.
    """
    if ordinal >= 60:
        ordinal -= 1  # Excel leap year bug, 1900 is not a leap year!
    return (_epoch0 + timedelta(days=ordinal)).replace(microsecond=0)
