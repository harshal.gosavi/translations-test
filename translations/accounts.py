# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Fanalytical Inc..

"""Accounts field translations."""

# Need to convert School_Grad_Year__pc string to date format
from datetime import datetime

import arrow
from dojson import Overdo
from dojson.utils import for_each_value, ignore_value
from loguru import logger

from translations.factory import translation_factory
from translations.wrangling import (
    basic_fix_string_value,
    clean_phone_number,
    create_external_id,
    is_valid_email,
    postprocess_account,
)

model = Overdo()
"""Account translations model."""


@model.over('external_ids', '^(Full_Account_ID__c|Banner_ID__c|Patron_ID__c)$')
@for_each_value
@ignore_value
def external_ids(self, key, value):
    """External ID."""
    if not value:
        logger.error('External ID {} not found', key)
        return None
    external_ids_model = {
        "Full_Account_ID__c": "salesforce",
        "Banner_ID__c": "banner",
        "Patron_ID__c": "paciolan",
    }
    if key in external_ids_model:
        return create_external_id(external_ids_model.get(key), value)


@model.over('creation_date', '^CreatedDate$')
@ignore_value
def creation_date(self, key, value):
    """Account creation date."""
    if not value:
        logger.debug('Creation date not found')
        return None

    try:
        return arrow.get(value).isoformat()
    except Exception:
        logger.error('Cannot convert {} into date for creation date.', value)
        return None


@model.over('modification_date', '^LastModifiedDate$')
@ignore_value
def modification_date(self, key, value):
    """Account creation date."""
    if not value:
        logger.debug('Modification date not found')
        return None

    try:
        return arrow.get(value).isoformat()
    except Exception:
        logger.error(
            'Cannot convert {} into date for modification date.', value
        )
        return None


@model.over('owner_id', '^OwnerId$')
@ignore_value
def owner_id(self, key, value):
    """Owner identifier, typically a sales representative."""
    if value is None:
        logger.debug('Owner ID not found')
        return
    owner_id_model = {
        "OwnerId": "salesforce",
    }
    if key and value is not None:
        return create_external_id(owner_id_model.get(key), value)


@model.over('last_name', '^LastName$')
@ignore_value
def last_name(self, key, value):
    """Last name coming from Account."""
    return basic_fix_string_value(value) if value else None


@model.over('first_name', '^FirstName$')
@ignore_value
def first_name(self, key, value):
    """First name coming from Account."""
    return basic_fix_string_value(value) if value else None


@model.over('gender', '^__TODO__$')
@ignore_value
def gender(self, key, value):
    """Account gender."""
    # Check enum: F,M
    return value if value else None


@model.over('birthdate', '^PersonBirthdate$')
@ignore_value
def birthdate(self, key, value):
    """Birthday coming from Account."""
    if not value:
        return None
    try:
        date_format = '%Y-%m-%d'
        try:
            datetime.strptime(value, date_format)
            return value
        except ValueError:
            return None

    except Exception:
        logger.error('Cannot convert {} into date for birthday', value)
        return None


@model.over('email', '^(PersonEmail|Secondary_Email__pc|Business_Email__c)$')
@for_each_value
@ignore_value
def email(self, key, value):
    """Email from account."""
    if not value or not is_valid_email(value):
        return None
    return value


@model.over('phone', '^(Phone|PersonHomePhone)$')
@for_each_value
@ignore_value
def phone(self, key, value):
    """Known phone numbers from Account."""
    value = clean_phone_number(value)
    if value:
        current_value = self.setdefault('phone', [])
        temp_list = self.get('phone', [])
        if value not in current_value:
            temp_list.append(value)


@model.over('type', '^(School_Alum__pc|Customer_Type__c|IsPersonAccount|'
                    'Account_Flag__c)$')
@ignore_value
def type(self, key, value):
    """Account type from Account."""
    # TODO this is an enum, check the schema
    if key == 'School_Alum__pc' and value is True:
        type_list = []
        type_list.append('alumni')
        return type_list
    if (key == 'Customer_Type__c' and value == 'YA'):
        type_list = []
        type_list.append('young alumni')
        type_list.append('alumni')
        return type_list
    if key == 'IsPersonAccount' and value is None:
        type_list = []
        type_list.append('company')
        return type_list
    if key == 'Account_Flag__c' and value is True:
        type_list = []
        type_list.append('elite')
        return type_list

    return None


@model.over('address', '^(BillingAddress|PersonMailingAddress|'
                       'PersonMailingState)$')
@for_each_value
@ignore_value
def address(self, key, value):
    """Known addresses."""
    if not value:
        return None
    if key == 'PersonMailingState':
        return None
    if 'country' not in value:
        value['country'] = 'US'
    if 'postalCode' in value:
        temp_val = value.pop('postalCode')
        value['postal_code'] = temp_val

    return value if value else None


@model.over('booster_level', '^Nova_Points__c$')
@ignore_value
def booster_level(self, key, value):
    """Account booster level."""
    return str(value) if value else None


@model.over('booster_start_date', '^__TODO__$')
@ignore_value
def booster_start_date(self, key, value):
    """Account booster start date."""
    if not value:
        return None
    try:
        return arrow.get(value).format('YYYY-MM-DD')
    except Exception:
        logger.error(
            'Cannot convert {} into date for booster start date.', value
        )
        return None


@model.over('faculty_staff_start_date', '^__TODO__$')
@ignore_value
def faculty_staff_start_date(self, key, value):
    """Account faculty staff start date."""
    if not value:
        return None
    try:
        return arrow.get(value).format('YYYY-MM-DD')
    except Exception:
        logger.error(
            'Cannot convert {} into date for faculty staff start date', value
        )
        return None


@model.over('letterwinner', '^(Men_s_Basketball_Full__c|Football_Full__c|'
                            'Women_s_Basketball_Full__c|Lacrosse_Full__c)$')
@for_each_value
@ignore_value
def letterwinner(self, key, value):
    """Letter winner from Account."""
    # TODO: set has_scholarship and alumni flags
    if not value:
        return None
    if key == 'Men_s_Basketball_Full__c':
        if value is True:
            data = []
            data.append('MBB')
            self['type'] = ['alumni']
            self['degree'] = [{'has_scholarship': True}]
            return data
    if 'Men_s_Basketball_Full__c' in self.get('letterwinner', []) \
            and value is True \
            and 'Football_Full__c' in self.get('letterwinner', []) \
            and value is True:
        data = []
        data.append('MBB', 'FB')
        self['type'] = ['alumni']
        self['degree'] = [{'has_scholarship': True}]
        return data
    if 'Men_s_Basketball_Full__c' in self.get('letterwinner', []) \
            and value is True \
            and 'Football_Full__c' in self.get('letterwinner', []) \
            and value is True \
            and 'Women_s_Basketball_Full__c' in self.get('letterwinner', []) \
            and value is True:
        data = []
        data.append('MBB', 'FB', 'WBB')
        self['type'] = ['alumni']
        self['degree'] = [{'has_scholarship': True}]
        return data
    if 'Men_s_Basketball_Full__c' in self.get('letterwinner', []) \
            and value is True \
            and 'Football_Full__c' in self.get('letterwinner', []) \
            and value is True \
            and 'Women_s_Basketball_Full__c' in self.get('letterwinner', []) \
            and value is True \
            and 'Lacrosse_Full__c' in self.get('letterwinner', []) \
            and value is True:
        data = []
        data.append('MBB', 'FB', 'WBB', 'ML')
        self['type'] = ['alumni']
        self['degree'] = [{'has_scholarship': True}]
        return data
    return None


@model.over('degree', '^School_Grad_Year__pc$')
@ignore_value
def degree(self, key, value):
    """Account degree."""
    if value:
        date_formats = ['%Y-%m-%d', '%Y', '%Y %b %d']
        for date_format in date_formats:
            try:
                dt = datetime.strptime(value, date_format)
                data = []
                data.append({'year': str(dt.year)})
                self['type'] = ['alumni']
                return data
            except ValueError:
                continue
        if value.isalpha():
            self['type'] = ['alumni']
        return None


@model.over('salary', '^AnnualRevenue$')
@ignore_value
def salary(self, key, value):
    """Income from Account."""
    if not value:
        return None
    try:
        if str(value).isdecimal():
            return int(value)
        return None
    except (ValueError, TypeError):
        logger.error('Cannot convert {} into integer for salary', value)
        return None


@model.over('company', '^Employer__c$')
@ignore_value
def company(self, key, value):
    """Company coming from Account.

    This field only applies if the account is a person. TODO: add check!
    """
    if not value:
        logger.error('Company {} not found', key)
        return None

    return value


@model.over('industry', '^Industry$')
@ignore_value
def industry(self, key, value):
    """Industry coming from Account."""
    if not value:
        logger.error('Industry {} not found', key)
        return None

    return value


@model.over('number_of_employees', '^NumberOfEmployees$')
@ignore_value
def number_of_employees(self, key, value):
    """Cast number of employees coming from account.

    This should only apply to company accounts. TODO: add check!
    """
    if not value:
        return None

    try:
        if str(value).isdecimal():
            self['type'] = ["company"]
            return int(value)
        else:
            self['type'] = ["company"]
        return None
    except (ValueError, TypeError):
        logger.error(
            'Cannot convert {} into integer for number of employees.', value
        )
        return None


@model.over('social', '^__TODO__$')
@for_each_value
@ignore_value
def social(self, key, value):
    """Account social networks."""
    return value if value else None


@model.over('contact_preference', '^__TODO__$')
@for_each_value
@ignore_value
def contact_preference(self, key, value):
    """Account contact preference."""
    return value if value else None


# Flags

@model.over('is_active', '^Inactive__pc$')
def is_active(self, key, value):
    """Is active."""
    if value is True:
        return False
    else:
        return True


@model.over('do_not_call', '^PersonDoNotCall$')
def do_not_call(self, key, value):
    """Do not call."""
    if value is True:
        return True
    else:
        return False
    if value is None:
        return False


@model.over('do_not_mail', '^__TODO__$')
def do_not_mail(self, key, value):
    """Do not mail."""
    if value is True or value == 'Yes':
        return True
    else:
        return False

    if value is None:
        return False


@model.over('do_not_email', '^Adobe_Opt_Out__pc$')
def do_not_email(self, key, value):
    """Do not email."""
    if value is True or value == 'Yes':
        return True
    else:
        return False

    if value is None:
        return False


translate = translation_factory(
    'test-uuid',
    model,
    id_='__TODO__',
    postprocess=postprocess_account,
)
"""Transform account data from source to Fanalytical schema."""
